��          �   %   �      P     Q  &   q     �     �  6   �     �  $     (   @     i  @   �     �  (   �       &     3   E  !   y  .   �     �     �  1   �  +   '  A   S     �  
   �     �  w  �  $   B  1   g     �  +   �  ?   �  (     1   =  5   o     �  M   �     	  '   *	     R	  +   i	  @   �	  0   �	  G   
     O
     d
  6   �
  2   �
  [   �
     F     d     t                                           	                                                          
                            Your changes were saved in: %s The rsync command failed Failed! %s %s succeeded %s was not enabled.  Cannot %s Available space, %s, is smaller than the margin of %s. Checking for existing files and Checking if there is enough room ... Insufficient space.  Have: %s. Need: %s. It returned an exit code of %s Manually save filesystem changes in %s file for persistent root. Missing rootfs version file %s Missing version ID in rootfs file system On VID error Performing rsync ... please be patient Please plug in the persistence device and try again Ready to sync file system changes See the rsync man page to look up its meaning. Shall we begin? Stopping at user's request The persistence device with UUID %s was not found Version id inside of %s file does not match You MUST run a program like this to save your filesystem changes. antiX Save Persistent Root mounted at persist device Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-01-01 11:17+0000
Last-Translator: aer, 2023-2024
Language-Team: German (http://app.transifex.com/anticapitalista/antix-development/language/de/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: de
Plural-Forms: nplurals=2; plural=(n != 1);
 Ihre Änderungen wurden gespeichert: %s Der rsync-Befehl ist fehlgeschlagen Fehler! %s %s erfolgreich %s war nicht freigeschaltet. Kann nicht  %s Der verfügbare Platz, %s ist kleiner als die Restmenge von %s. Überprüfung auf vorhandene Dateien und Überprüfe, ob genügend Platz vorhanden ist ... Zu wenig Speicherplatz. Vorhanden: %s. Benötigt: %s. Es gab einen Exitcode %s Manuelle Dateisystemänderungen speichern in %s Datei für persistentes root. rootfs-Versionsdatei %s fehlt Versions-ID in rootfs-Dateisystem fehlt auf VID ist ein Fehler Führe rsync aus ... bitte ein wenig Geduld Persistenz-Gerät bitte neu hineinstecken und nochmals versuchen Bereit Dateisystemänderungen zu synchronisieren In den rsync-Handbuchseiten können Sie nachschlagen, was das bedeutet. Sollen wir anfangen? Auf Benutzerwunsch gestoppt Das Persistenz-Gerät mit UUID %s wurde nicht gefunden Version-ID im Datei %s stimmt damit nicht überein Sie MÜSSEN ein Programm wie dieses ausführen, um Änderungen am Dateisystem zu speichern. Die root-Persistenz speichern eingehängt auf Persistenz-Gerät 